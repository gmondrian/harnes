//
//  ContentView.swift
//  Harnes
//
//  Created by GENA on 22.08.2019.
//  Copyright © 2019 GENA. All rights reserved.
//

import SwiftUI
import CoreData

/* @ObservedObject does not work */
@objc(ManagedEntity) public class ManagedEntity: NSManagedObject {
    @NSManaged public var name: String // = "Managed Entity"
    @NSManaged public var enabled: Bool // = false
}

// @ObservedObject works */
final class ObservableEntity: ObservableObject {
    @Published var name = "Observable Entity"
    @Published var enabled = false
}

struct ContentView: View {
    @ObservedObject var managedEntity: ManagedEntity
    @ObservedObject var observableEntity: ObservableEntity
    var body: some View {
        VStack {
            Toggle("ManagedEntity.enabled", isOn: $managedEntity.enabled)
            TextField("ManagedEntity.name", text: $managedEntity.name)
                .disabled(!managedEntity.enabled)
                .textFieldStyle(RoundedBorderTextFieldStyle())

            Toggle("ObservableEntity.enabled", isOn: $observableEntity.enabled)
            TextField("ObservableEntity.name", text: $observableEntity.name)
                .disabled(!observableEntity.enabled)
                .textFieldStyle(RoundedBorderTextFieldStyle())
        }
        .padding(.horizontal)
    }
}

extension AppDelegate {
    var defaultManagedEntity: ManagedEntity {
        return ManagedEntity(context: persistentContainer.viewContext)
    }
    var defaultObservableEntity: ObservableEntity {
        return ObservableEntity()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError("Unable to get delegate.")
        }
        return ContentView(managedEntity: delegate.defaultManagedEntity, observableEntity:delegate.defaultObservableEntity)
    }
}

